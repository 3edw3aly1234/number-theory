package problem1;

public class Algorithm_2 {
	
	public int ApowerBmodN(int a, int b, int n) {
		final int x = a % n;
		int result = 1;
		for (int i = 0; i < b; i++) {
			result = result * x % n;
		}
		return result;
	}
}

package problem1;

public class Algorithm_3 {
	
	public int ApowerBmodN(int a, int b, int n) {
		int x = a % n;
		int y = b;
		int result = 1;
		while (y != 0) {
			if (y % 2 != 0) {
				result = result * x % n;
			}
			y >>= 1;
			x = x * x % n;
		}
		return result;
	}
}

package problem1;

public class Algorithm_4 {
	
	public int ApowerBmodN(int a, int b, int n) {
		int x = a % n;
		int result = 1;
		final String binary = Integer.toBinaryString(b);
		for (int i = 0; i < binary.length(); i++) {
			char temp = binary.charAt(i);
			result = result * result % n;
			if (temp == '1') {
				result = result * x % n;
			}
		}
		return result;
	}
}

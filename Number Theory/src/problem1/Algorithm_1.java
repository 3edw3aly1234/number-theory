package problem1;

public class Algorithm_1 {
	
	public int ApowerBmodN(int a, int b, int n) {
		int result = 1;
		for(int i = 0; i < b; i++) {
			result *= a;
		}
		result %= n;
		return result;
	}
}

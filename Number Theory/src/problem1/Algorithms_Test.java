package problem1;

public class Algorithms_Test {

	public static void main(String[] args) {
		Algorithm_1 alg_1 = new Algorithm_1();
		final int res1 = alg_1.ApowerBmodN(2, 3, 6); // 2 ^ 3 mod 6 = 2
		System.out.println(res1);
		
		Algorithm_2 alg_2 = new Algorithm_2();
		final int res2 = alg_2.ApowerBmodN(2, 3, 6); // 2 ^ 3 mod 6 = 2
		System.out.println(res2);
		
		Algorithm_3 alg_3 = new Algorithm_3();
		final int res3 = alg_3.ApowerBmodN(2, 3, 6); // 2 ^ 3 mod 6 = 2
		System.out.println(res3);
		
		Algorithm_4 alg_4 = new Algorithm_4();
		final int res4 = alg_4.ApowerBmodN(2, 3, 6); // 2 ^ 3 mod 6 = 2
		System.out.println(res4);
	}

}

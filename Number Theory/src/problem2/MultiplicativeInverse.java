package problem2;

public class MultiplicativeInverse {
	
	final public long multiplicativeInverse(final long a, final long n) {
		long u = a, v = n;
		long x, x1 = 1, x2 = 0;
		long q, r;
		while (u != 1) {
			q = v / u;
			r = v - q * u;
			x = x2 - q * x1;
			v = u;
			u = r;
			x2 = x1;
			x1 = x;
		}
		long mi = x1 % n;
		if (mi < 0) {
			mi += n;
		}
		return mi;
	}
}

package problem2;

public class MultiplicativeInverseTest {

	public static void main(String[] args) {
		MultiplicativeInverse MI = new MultiplicativeInverse();
		int a = 17, n = 22;
		long mi = MI.multiplicativeInverse(a, n);
		System.out.println(mi);
		a = 100; n = 17;
		mi = MI.multiplicativeInverse(a, n);
		System.out.println(mi);
		a = 25; n = 39;
		mi = MI.multiplicativeInverse(a, n);
		System.out.println(mi);
	}

}

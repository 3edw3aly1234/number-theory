package problem3;
import problem2.MultiplicativeInverse;

public class ChineseRemainderTheorem {
	
	public long[] mapping(final long[] m, final long A) {
		final int lengthOfArray = m.length;
		final long[] a = new long[lengthOfArray];
		for (int i = 0; i < lengthOfArray; i++) {
			a[i] = A % m[i];
		}
		return a;
	}
	
	public long reverseMapping(final long[] m, final long[] a) {
		if (m.length != a.length) {
			throw new RuntimeException("Two arrays of different lengths!");
		}
		final int lengthOfArray = m.length;
		long n = 1;
		for (int i = 0; i < lengthOfArray; i++) {
			n *= m[i];
		}
		final long[] productButNum = new long[lengthOfArray];
		for (int i = 0; i < lengthOfArray; i++) {
			productButNum[i] = n / m[i];
		}
		MultiplicativeInverse MI = new MultiplicativeInverse();
		final long[] mmi = new long[lengthOfArray];
		for (int i = 0; i < lengthOfArray; i++) {
			mmi[i] = MI.multiplicativeInverse(productButNum[i], m[i]);
		}
		long A = 0;
		for (int i = 0; i < lengthOfArray; i++) {
			A += a[i] * productButNum[i] * mmi[i];
		}
		A %= n;
		return A;
	}
}

package problem3;

import java.util.Arrays;

public class ChineseRemainderTheoremTest {

	public static void main(String[] args) {
		ChineseRemainderTheorem CRT = new ChineseRemainderTheorem();
		long[] m = {3, 4, 5};
		long A = 11;
		long[] a = CRT.mapping(m, A);
		System.out.println(Arrays.toString(a));
		A = CRT.reverseMapping(m, a);
		System.out.println(A);
	}

}
